"use strict";

const FRONTEND_URL = "http://localhost:8081/";
const BACKEND_URL = "http://localhost:8080/";

const responseTextElement = document.getElementById("responseText");

function setResponseText(responseText) {
    responseTextElement.textContent = responseText;
}

function submitJSONToBackend(json, backendPath, responseConsumer) {
    fetch(BACKEND_URL + backendPath, {
        method: "POST",
        body: json,
        credentials: "include",
        headers: {
            "Content-Type": "application/json"
        },
    }).then(responseConsumer, function (error) {
        console.log(error);
        setResponseText(error.message);
    });
}

function submitFormDataToBackend(formData, backendPath, responseConsumer) {
    submitJSONToBackend(JSON.stringify(Object.fromEntries(formData.entries())), backendPath, responseConsumer);
}

function submitFormToBackend(formElement, backendPath, responseConsumer) {
    submitFormDataToBackend(new FormData(formElement), backendPath, responseConsumer);
}

async function setResponseTextConsumer(response) {
    setResponseText(await response.text());
}

function getAccessLevelString(accessLevelID) {
    let accessLevelString;
    switch (accessLevelID) {
        case -1:
            accessLevelString = "guest";
            break;
        case 0:
            accessLevelString = "student";
            break;
        case 1:
            accessLevelString = "member";
            break;
        case 2:
            accessLevelString = "teacher";
            break;
        case 3:
            accessLevelString = "admin";
            break;
        default:
            accessLevelString = "guest"
    }
    return accessLevelString;
}
