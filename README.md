# NHS Frontend

A simple testing, static-HTML frontend for the [NHS Backend](https://www.gitlab.com/scr0nch/nhs-backend) server.

Currently implemented features:

 - Homepage
 - NHS member registration
 - Email validation
 - Login
 - Access code generation

Running:

This project can be run by any http server implementation. An easy option is the npm package [`http-server`](https://www.npmjs.com/package/http-server):

    npm install http-server
    # run in cloned repository;s root directory
    # note that the server must run on port 8081 because the nhs-backend server runs on port 8080.
    http-server -p 8081 .
